package net.diegoquirino.calculator.service;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@DisplayName("UNIT: Testes do Serviço de Cálculo do Fator de Desconto")
public class DiscountFactorCalculationServiceUnitTests {

    @Autowired
    DiscountFactorCalculationService driver;

    String clientType;

    @Nested
    @DisplayName("Quando o Cliente é do tipo 'A'")
    class ClientTypeA {

        @BeforeEach
        public void setup() {
            clientType = "A";
        }

        @Test
        @DisplayName("E Quantidade de 1 até 99 ENTÃO o desconto é 10%")
        public void whenClientAQuantityLessThen100ThenDiscount10Percent() {
            // AAA Pattern => Arrange, then Act, finally Assert
            /* Arrange */
            Integer quantityMin = 1;
            Integer quantityMax = 99;
            /* Act */
            Double resultMin = driver.calculate(clientType, quantityMin);
            Double resultMax = driver.calculate(clientType, quantityMax);
            /* Assert */
            assertThat(resultMin, is(0.90));
            assertThat(resultMax, is(0.90));
        }

    }

    @AfterEach
    public void tearDown() {
        clientType = null;
    }

}
