package net.diegoquirino.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class DiscountFactorCalculationImplService implements DiscountFactorCalculationService {
    @Override
    public Double calculate(String clientType, Integer quantity) {
        if(clientType.equals("A") && (quantity >=1 && quantity < 100) ) {
            return 0.90;
        }
        if(clientType.equals("A") && (quantity >=100 && quantity < 1000) ) {
            return 0.95;
        }
        return null;
    }
}
